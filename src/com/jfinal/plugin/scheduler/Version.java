package com.jfinal.plugin.scheduler;

public class Version {
	/**
	 * ----日期----版本-------更新说明------------------
	 * 2016-11-03 2.2.0  重构代码，支持注解方式。
	 * 2017-04-05 3.0.0  重构代码，兼容Spring的Scheduled注解。
	 */
	public final static int MajorVersion    = 3;
    public final static int MinorVersion    = 4;
    public final static int RevisionVersion = 0;

    public static final String VERSION    = Version.MajorVersion + "." + Version.MinorVersion + "." + Version.RevisionVersion;
    public static final String RELEASE_AT = "2017-04-25";
    public static final String RELEASE_NOTE="兼容jfinal3.4";
    
    private Version() {
    }
}
